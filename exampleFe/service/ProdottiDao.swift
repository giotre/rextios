//
//  ProdottiDao.swift
//  exampleFe
//
//  Created by Giovanni on 25/09/17.
//  Copyright © 2017 Giovanni. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ProdottiDao{
    
    static let shared = ProdottiDao() // proprietà per ottenere la classe in modalità Singleton
    
    private var context: NSManagedObjectContext // riferimento al contenitore degli oggetti (ManagedObject) salvati in memoria
    
    private init() {
        let application = UIApplication.shared.delegate as! AppDelegate // recupero l'istanza dell'AppDelegate dell'applicazione
        self.context = application.persistentContainer.viewContext // recupero il ManagedObjectContext dalla proprietà persistantContainer presente nell'App Delegate
    }
    
    func idIncrement() -> Int16 {
        struct StaticVars {
            static var counter = 0
        }
        StaticVars.counter += 1
        return Int16(StaticVars.counter)
    }
    
    func addProduct(name: String, desc: String) {
        /*
         Per creare un oggetto da inserire in memoria è necessario creare un riferimento all'Entity (NSEntityDescription) da cui si copierà la struttura di base
         */
        let entity = NSEntityDescription.entity(forEntityName: "Prodotti", in: self.context)
        
        /*
         creiamo un nuovo oggetto NSManagedObject dello stesso tipo descritto dalla NSEntityDescription
         che andrà inserito nel context dell'applicazione
         */
        let newProduct = Prodotti(entity: entity!, insertInto: self.context)
        newProduct.name = name
        newProduct.desc = desc
        newProduct.chiave = Int16(idIncrement())
        
        do {
            try self.context.save() // la funzione save() rende persistente il nuovo oggetto (newLibro) in memoria
        } catch let errore {
            print("[CDC] Problema salvataggio Product: \(newProduct.name!) in memoria")
            print("  Stampo l'errore: \n \(errore) \n")
        }
        
      
        print("[CDC] Libro \(newProduct.name!) salvato in memoria correttamente")
    }
    
    func loadBooksFromAuthor(author: String) -> [Prodotti]  {
        print("[CDC] Recupero tutti i prodotti dal context ")
        
        let request: NSFetchRequest<Prodotti> = NSFetchRequest(entityName: "Prodotti")
        request.returnsObjectsAsFaults = false
        
        let predicate = NSPredicate(format: "autore = %@", author) // L'oggetto predicate permette di aggiungere dei filtri sulla NSFetchRequest
        request.predicate = predicate
        
        let books = self.loadBooksFromFetchRequest(request: request)
        return books
    }
    
    func loadBooksAll() -> [Prodotti]  {
        print("[CDC] Recupero tutti i prodotti dal context ")
        
        let request: NSFetchRequest<Prodotti> = NSFetchRequest(entityName: "Prodotti")
        request.returnsObjectsAsFaults = false
        
       
        let books = self.loadBooksFromFetchRequest(request: request)
        return books
    }
    
    /*
     La funzione restituisce un array di libri dopo aver eseguito la request
     */
    private func loadBooksFromFetchRequest(request: NSFetchRequest<Prodotti>) -> [Prodotti] {
        var array = [Prodotti]()
        do {
            array = try self.context.fetch(request)
            
            guard array.count > 0 else {print("[CDC] Non ci sono elementi da leggere "); return []}
            
            for x in array {
                print("[CDC] Libro \(x.chiave) - Autore \(x.desc!)")
            }
            
        } catch let errore {
            print("[CDC] Problema esecuzione FetchRequest")
            print("  Stampo l'errore: \n \(errore) \n")
        }
        
        return array
    }
    
    func loadBookFromName(name: String) -> Prodotti {
        let request: NSFetchRequest<Prodotti> = NSFetchRequest(entityName: "Prodotti")
        request.returnsObjectsAsFaults = false
        
        let predicate = NSPredicate(format: "name = %@", name)
        request.predicate = predicate
        
        let books = self.loadBooksFromFetchRequest(request: request)
        return books[0]
    }
    
    func loadBookFromId(chiave: Int16) -> Prodotti {
        let request: NSFetchRequest<Prodotti> = NSFetchRequest(entityName: "Prodotti")
        request.returnsObjectsAsFaults = false
        
        let predicate = NSPredicate(format: "chiave == %i", chiave)
        request.predicate = predicate
        
        let books = self.loadBooksFromFetchRequest(request: request)
        return books[0]
    }
    
    func deleteBookById(chiave: Int16) {
        let book = self.loadBookFromId(chiave: Int16(chiave))
        self.context.delete(book)
        
        do {
            try self.context.save()
        } catch let errore {
            print("[CDC] Problema eliminazione libro ")
            print("  Stampo l'errore: \n \(errore) \n")
        }
    }
}
