//
//  ViewController.swift
//  exampleFe
//
//  Created by Giovanni on 22/09/17.
//  Copyright © 2017 Giovanni. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var saveProd: UIButton!
    @IBOutlet weak var txName: UITextField!
    @IBOutlet weak var txDesc: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func saveProd(_ sender: Any) {
        guard !self.txName.text!.isEmpty else {return}
        guard !self.txDesc.text!.isEmpty else {return}
        
        let name = self.txName.text!
        let desc = self.txDesc.text!
        
        ProdottiDao.shared.addProduct(name: name, desc: desc)
    }
   
    
    @IBAction func printBooks_clicked(_ sender: UIButton) {
        //CoreDataController.sharedIstance.loadAllBooks()
        ProdottiDao.shared.loadBooksFromAuthor(author: "Dante Alighieri")
    }
  

}

