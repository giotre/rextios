//
//  Prodotti+CoreDataProperties.swift
//  exampleFe
//
//  Created by Giovanni on 26/09/17.
//  Copyright © 2017 Giovanni. All rights reserved.
//
//

import Foundation
import CoreData


extension Prodotti {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Prodotti> {
        return NSFetchRequest<Prodotti>(entityName: "Prodotti")
    }

    @NSManaged public var desc: String?
    @NSManaged public var chiave: Int16
    @NSManaged public var name: String?

}
