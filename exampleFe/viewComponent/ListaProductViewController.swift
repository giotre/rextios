//
//  ListaProductViewController.swift
//  exampleFe
//
//  Created by Giovanni on 25/09/17.
//  Copyright © 2017 Giovanni. All rights reserved.
//

import UIKit

class ListaProductViewController: UITableViewController {
 
    var listProd = ProdottiDao.shared.loadBooksAll()
 

    override func viewDidLoad(){
        super.viewDidLoad()
       
       
        //print("chiamoView")
    }
 
    override func viewWillAppear(_ animated: Bool) {
        listProd = ProdottiDao.shared.loadBooksAll()
        
        super.viewWillAppear(animated)
        self.tableView.reloadData()
       
        //print("chiamoRefresh")
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "DataProdCel", for: indexPath) as! CustomTableViewCell
            print("chiamoView")
        
            listProd = ProdottiDao.shared.loadBooksAll()
            let content = listProd[indexPath.row]
            //let text = listaProdotti[indexPath.row]
            print(content)
            cell.labCel.text = content.desc
        return cell
    }
    
   
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listProd.count
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            
            var tempListProd = ProdottiDao.shared.loadBookFromId(chiave: listProd[indexPath.row].chiave)
            
            ProdottiDao.shared.deleteBookById(chiave: Int16(tempListProd.chiave))
            listProd.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
}
